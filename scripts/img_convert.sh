#!/bin/sh
QCOWORG="${OUTPUTDIR}/packer-ubuntu"
QCOW="${OUTPUTDIR}/ubuntu.qemu.qcow2"

qemu-img convert -c -p -f qcow2 ${QCOWORG} -O qcow2 ${QCOW}
rm -rf ${QCOWORG}
rm -rf ./images