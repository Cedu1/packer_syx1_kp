packer {
  required_plugins {
    qemu = {
      version = ">= 1.0.1"
      source  = "github.com/hashicorp/qemu"
    }
    virtualbox = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/virtualbox"
    }
  }
}

variable "password" {
  type      = string
  default   = "vagrant"
  sensitive = true
}


source "qemu" "ubuntu" {
    iso_url         = "https://releases.ubuntu.com/jammy/ubuntu-22.04.2-live-server-amd64.iso"
    iso_checksum    = "sha256:5e38b55d57d94ff029719342357325ed3bda38fa80054f9330dc789cd2d43931"
    communicator    = "ssh"
    ssh_username    = "root"
    ssh_timeout     = "60m"
    ssh_password    = var.password
    boot_wait       = "10s"
    boot_command    = [
        "e<down><down><down><end><left><left><left><left>",
        " autoinstall ds=nocloud-net\\;seedfrom=http://{{ .HTTPIP }}:{{ .HTTPPort }}/jammy/ <wait1>",
        "<f10>"
    ]
    disk_interface  = "virtio"
    net_device      = "virtio-net"
    format          = "qcow2"
    disk_size       = "10G"
    http_content    = {
        "/jammy/user-data" = file("${abspath(path.root)}/http/jammy/user-data"),
        "/jammy/meta-data" = file("${abspath(path.root)}/http/jammy/meta-data"),
    }
    qemuargs = [["-m", "2048M"],["-smp", "2"],["-rtc", "base=localtime,driftfix=slew"],["-cpu", "host,hv_relaxed,hv_vapic,hv_spinlocks=0x1fff"],[ "-global", "virtio-blk-pci.physical_block_size=4096" ]]
    output_directory = "./images"
}

build {
  name    = "ubuntu"
  sources = ["source.qemu.ubuntu"]

  provisioner "shell" {
    inline = [
      "sleep 30",  # Wait for network initialization (optional)
      "sudo apt-get update",
      "sudo apt-get install -y apache2",
      # Additional provisioning steps can be added here
    ]
  }

  post-processor "shell-local" {
    environment_vars = ["OUTPUTDIR='./images"]
    script           = "scripts/img_convert.sh"
  }
}